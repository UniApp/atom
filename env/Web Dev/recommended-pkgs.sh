# From: https://www.udemy.com/course/the-complete-web-development-bootcamp/learn/lecture/12371312#overview
Recommended=(
atom-beautify
atom-ternjs
autoclose-html
emmet
csslint
pigments
language-ejs
)

Optional=(
atom-html-preview
Sublime-Style-Column-Selection
linter-eslint
)

dependency=(
linter
linter-ui-default
intentions
busy-signal
)


install_atom_pkg(){
  pkg=("$@")
  for i in "${pkg[@]}"
  do :
    echo $i
    apm install $i
  done
}

install_atom_pkg "${Recommended[@]}"
install_atom_pkg "${Optional[@]}"
install_atom_pkg "${dependency[@]}"

# guide https://flight-manual.atom.io/using-atom/sections/atom-packages/
